#include "Ball.h"
#include <cmath>

Ball::Ball() {

}

Ball::Ball(float x, float y, float radius):x(x), y(y), radius(radius) {
	dir = igvPunto3D(0, 1, 0);
	centro = igvPunto3D(x, y, 0);
	speed = 0.0015 *2;
}
//Comprueba si hay colisiones
bool Ball::checkColission(Block& block) {
	float squaredDistance = squareDistance(centro, block);
	return squaredDistance <= (radius * radius);
}
//Calcula la distancia de la bola a un bloque, usada para comprobar la colisi�n
float Ball::squareDistance(const igvPunto3D& p, Block& aabb) {
	//Funcion que copio de studiofreya
	auto check = [&](
		const double pn,
		const double bmin,
		const double bmax) -> double
	{
		double out = 0;
		double v = pn;

		if (v < bmin)
		{
			double val = (bmin - v);
			out += val * val;
		}

		if (v > bmax)
		{
			double val = (v - bmax);
			out += val * val;
		}

		return out;
	};

	//Distancia al cuadrado

	float sq = 0.0;

	sq += check(p[0], aabb.mini()[0], aabb.maxi()[0]);
	sq += check(p[1], aabb.mini()[1], aabb.maxi()[1]);
	   
	return sq;
}
//Calcula el �ngulo en el que rebotar�a la bola
void Ball::calculateRebound(Block& block) {
	// v' = v - 2(v*n) x n
	//Soluciona un bug rar�simo
	//cout << block.blockId << endl;
	if (pastBlock.blockId == block.blockId) {
		if (block.blockId == 5) {
			x -= radius/2;
		}
		else if (block.blockId == 4) {
			x += radius/2;
		}
		else {
			if (this->y > block.getY())
				y += radius/2;
			if (this->y < block.getY())
				y -= radius/2;
		}
	}
	else {
		pastBlock = block;
	}

	//Se actualiza la normal del bloque
	updateNormal(block);

	//cout << block.getNormal()[0] << ", " << block.getNormal()[1] << endl;

	//Se calcula el producto escalar entre los vectores (v*n)
	float producto = dir[0] * block.getNormal()[0] + dir[1] * block.getNormal()[1];	
	igvPunto3D resultado;

	//Se calcula cada una de las coordenadas del vector resultante independientemente
	resultado[0] = dir[0] - 2 * producto * block.getNormal()[0];
	resultado[1] = dir[1] - 2 * producto * block.getNormal()[1];

	//Se cambia el vector direcci�n de la bola, para evitar que rebote siempre de forma vertical, en caso de qeu la x sea 0, se le dar� un valor aleatorio
	if (resultado[0] == 0)
		resultado[0] = ((double)rand() / (RAND_MAX)) + 1;

	//Se comprueba si se est� chocando contra una esquina
	//Si choca desde la esquina superior izquierda
	if (this->getX() < (block.getX() - block.getWidth() / 2) && this->getY() > (block.getY() + block.getHeight() / 2)) {
		igvPunto3D dir(-1, 1, 0);		
		this->dir = dir;
	}
	//Si choca desde la esquina superior derecha
	else if (this->getX() > (block.getX() + block.getWidth() / 2) && this->getY() > (block.getY() + block.getHeight() / 2)) {
		igvPunto3D dir(1, 1, 0);
		this->dir = dir;
	}
	//Si choca desde la esquina inferior izquierda
	if (this->getX() < (block.getX() - block.getWidth() / 2) && this->getY() < (block.getY() + block.getHeight() / 2)) {
		igvPunto3D dir(-1, -1, 0);
		this->dir = dir;
	}
	//Si choca desde la esquina interior derecha
	else if (this->getX() > (block.getX() + block.getWidth() / 2) && this->getY() < (block.getY() + block.getHeight() / 2)) {
		igvPunto3D dir(1, -1, 0);
		this->dir = dir;
	}

	//Normaliza el vector resultado para que no se acelere la bola
	if (resultado[0] > 1 || resultado[0] < -1)
		resultado[0] /= sqrt(resultado[0] * resultado[0] + resultado[1] * resultado[1]);
	if (resultado[1] > 1 || resultado[1] < -1)
		resultado[1] /= sqrt(resultado[0] * resultado[0] + resultado[1] * resultado[1]);
	dir = resultado;
}

void Ball::Update(float deltaTime) {
	//Se mueve la bola en funci�n de su vector velocidad 
	x += dir[0] * speed * deltaTime;
	y += dir[1] * speed * deltaTime;
	centro = igvPunto3D(x, y, 0);
}

void Ball::updateNormal(Block& block) {
	//Si la bola est� a la izquierda y a la misma altura que el bloque, la normal es (-1, 0)
	if (this->x < block.getX() - block.getWidth() / 2 && this->y > block.getY() - block.getHeight() / 2 && this->y < block.getY() + block.getHeight() / 2) {
		block.setNormal(igvPunto3D(-1, 0, 0));
	}
	//Si la bola est� a la derecha y a la misma altura que el bloque, la normal es (1, 0)
	else if (this->x > block.getX() - block.getWidth() / 2 && this->y > block.getY() - block.getHeight() / 2 && this->y < block.getY() + block.getHeight() / 2) {
		block.setNormal(igvPunto3D(1, 0, 0));
	}
	//Si la bola est� arriba del bloque y entre su anchura, la normal es (0, 1)
	else if (this->y > block.getY() - block.getHeight() / 2 && this->x > block.getX() - block.getWidth() / 2 && this->x < block.getX() + block.getWidth() / 2) {
		block.setNormal(igvPunto3D(0, 1, 0));
	}
	else {
		block.setNormal(igvPunto3D(0, -1, 0));
	}
}