#pragma once
#include "Ball.h"
#include "Block.h"
#include "Paddle.h"
#include <time.h>
#include <vector>

using namespace std;

class Game {
	//Necesario para saber si se debe detener el juego
	enum GameState{Active, Paused, Ended};
private:
	//Reloj del juego
	clock_t oldTime;
	float fps;
	float deltaTime;
	
	//Estado del juego
	GameState gameState;

	//Objetos del juego
	Ball ball;
	Paddle paddle;	
	vector<Block> walls;
	vector<Block> blocks;

	//Posiciones iniciales
	float posX;
	float posY;

	int contador;

	//Metodos
	void checkCollisions();
	void createWalls();
	void createBlocks();
public:
	Game();
	void Update();
	float getDeltaTime() { return deltaTime; };
	Ball& getBall() { return ball; };
	Paddle& getPaddle() { return paddle; };
	vector<Block>& getWalls() { return walls; };
	vector<Block>& getBlocks() { return blocks; };
	GameState getGameState() { return gameState; };
	void setGameState(int gamestate) { gameState = (GameState)gamestate; };

	void checkGameOver();
	void restart();
};