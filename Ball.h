#pragma once
#define _USE_MATH_DEFINES
#include "igvPunto3D.h"
#include "Block.h"
#include "Paddle.h"
#include <cmath>

#include <iostream>
using namespace std;

class Ball
{
private:
	Block pastBlock;

	float speed; //Velocidad de la bola
	igvPunto3D dir; //Vector sentido de la bola
	igvPunto3D centro; //Posici�n del centro de la bola
	float x; //Posici�n x de la bola
	float y; //Posici�n y de la bola
	float radius;	
	//M�todo interno utilizado para comprobar las colisiones de la bola
	float squareDistance(const igvPunto3D &p, Block &aabb);

public:
	Ball();
	Ball(float x, float y, float radius);
	//Actualiza la posici�n de la bola
	void Update(float deltaTime);
	//Comprueba si la bola ha chocado contra un bloque
	bool checkColission(Block& block);
	//Calcula el �ngulo al que se dirige la bola tras chocar contra un bloque
	void calculateRebound(Block& block);
	//Recalcula la normal del bloque
	void updateNormal(Block& block);

	float getX() const { return x; }
	float getY() const { return y; }
	float getRadius()const { return radius; };	
	igvPunto3D getDir() const { return dir; }
};

