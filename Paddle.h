#pragma once
#include "Block.h"
class Paddle
{
private:
	float speed;
	Block paddle;
public:
	Paddle();

	Block& getBlock() { return paddle; };
	void moveLeft(float deltaTime);
	void moveRight(float deltaTime);
};
