#include "Game.h"

Game::Game() {	
	gameState = Paused;
	ball = Ball(1.0, -1, 0.15);
	paddle = Paddle();
	posX = 2.5;
	posY = 5.0;
	contador = 0;
	createWalls();
	createBlocks();
}
//Ejecuta el juego, cada frame invocar� a este m�todo
void Game::Update() {	
		//Actualiza deltaTime
		deltaTime = clock() - oldTime;
		fps = (1 / deltaTime) * 1000;
		oldTime = clock();

	if (gameState == Active) {
		//Comprueba colisiones de la bola
		checkCollisions();
		//Actualiza la posici�n de la bola
		ball.Update(deltaTime);

		checkGameOver();
		
	}
	
}
//Comprueba todas las colisiones que puedan darse durante la partida
void Game::checkCollisions() {
	//Se comprueban todos los bloques del juego
	for (int i = 0; i < blocks.size(); i++) {
		//Si choca contra un bloque
		if (blocks[i].isActive() && ball.checkColission(blocks[i])) {
			//Se modifica la direcci�n en la que se mueve la bola
			ball.calculateRebound(blocks[i]);
			blocks[i].setActive(false);
			contador++;
		}
	}	
	//Si choca contra un muro
	for (int i = 0; i < walls.size(); i++) {
		//Si choca contra un bloque
		if (ball.checkColission(walls[i])) {
			//Se modifica la direcci�n en la que se mueve la bola
			ball.calculateRebound(walls[i]);			
		}
	}
	//Si choca contra la pala
	if (ball.checkColission(paddle.getBlock())) {
		//Se modifica la direcci�n en la que se mueve la bola
		ball.calculateRebound(paddle.getBlock());
	}
}
//Crea el conjunto de bloques destruibles
void Game::createBlocks() {
	//esto es una prueba
	for (int y = 0; y < 4; y++) {
		for (int i = 0; i < 6; i++) {
			igvPunto3D normal(0, -1, 0);
			Block a(-1.7 + (0.7 * i), 2.5 + (y*0.5), 0.5, 0.7, normal);
			blocks.push_back(a);
		}
	}
	
}
//Crea los muros que contienen a la bola
void Game::createWalls() {
	//Se crean los muros
	igvPunto3D normal(0, -1, 0);
	Block up(0, posY, 1, 6, normal);
	walls.push_back(up);

	normal = igvPunto3D(1, 0, 0);
	Block left(-posX, posY/2, 20, 1, normal);
	walls.push_back(left);

	normal = igvPunto3D(-1, 0, 0);
	Block right(posX, posY/2, 20, 1, normal);
	walls.push_back(right);
	//Muro inferior para contener la bola, este se eliminar�
	//normal = igvPunto3D(0, 1, 0);
	Block down(0, -posY/2, 1, 20, normal);
	//walls.push_back(down);
}

void Game::checkGameOver() {
	if (ball.getY() < (paddle.getBlock().getY() - 0.7) || contador == 24)
		this->gameState = Ended;

}

void Game::restart() {	
	ball = Ball(1.0, -1, 0.15);
	paddle = Paddle();
	posX = 2.5;
	posY = 5.0;

	blocks.empty();

	Block::id = 0;

	createBlocks();
	gameState = Active;
}