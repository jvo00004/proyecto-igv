#include <cstdlib>
#include <stdio.h>

#include "igvEscena3D.h"
#include "igvFuenteLuz.h"
#include "igvMaterial.h"
#include "igvTextura.h"

// Metodos constructores 

igvEscena3D::igvEscena3D () {	
}

igvEscena3D::~igvEscena3D() {
}


// Metodos publicos 
void pintar_ejes(void) {
  GLfloat rojo[]={1,0,0,1.0};
  GLfloat verde[]={0,1,0,1.0};
  GLfloat azul[]={0,0,1,1.0};

  glMaterialfv(GL_FRONT,GL_EMISSION,rojo);
	glBegin(GL_LINES);
		glVertex3f(1000,0,0);
		glVertex3f(-1000,0,0);
	glEnd();

  glMaterialfv(GL_FRONT,GL_EMISSION,verde);
	glBegin(GL_LINES);
		glVertex3f(0,1000,0);
		glVertex3f(0,-1000,0);
	glEnd();

  glMaterialfv(GL_FRONT,GL_EMISSION,azul);
	glBegin(GL_LINES);
		glVertex3f(0,0,1000);
		glVertex3f(0,0,-1000);
	glEnd();
}

void igvEscena3D::visualizar(void) {
	// crear el modelo
	glPushMatrix(); // guarda la matriz de modelado
	//Crea la luz direccional
	luzDireccional();
	//Se dibuja la bola
	pintarBola();
	//Se dibujan los muros
	pintarMuros();
	//Se dibujan los bloques
	pintarBloques();
	//Se dibuja la pala
	pintarPala();
	glPopMatrix (); // restaura la matriz de modelado
}
// Crea una luz direccional que iluminará la escena
void igvEscena3D::luzDireccional() {
	igvPunto3D pos(0, 0, 5);
	igvColor cAmb(1, 1, 1);
	nuevaLuz = igvFuenteLuz(GL_LIGHT0, pos, cAmb, cAmb, cAmb, 0.1, 0.05, 0.05);
	nuevaLuz.aplicar();
}
// Pinta la bola que destruirá los bloques
void igvEscena3D::pintarBola() {
	//Color de la bola
	GLfloat params[] = { 1, 0.3, 0 };
	glMaterialfv(GL_FRONT, GL_EMISSION, params);

	glPushMatrix();
		glTranslatef(juego.getBall().getX(), juego.getBall().getY(), 0);
		glutSolidSphere(juego.getBall().getRadius(), 100, 100);
	glPopMatrix();
}
// Pinta los muros que contienen la bola
void igvEscena3D::pintarMuros() {
	//Color de los muros
	GLfloat params[] = { 0, 0, 1 };
	glMaterialfv(GL_FRONT, GL_EMISSION, params);

	for (int i = 0; i < juego.getWalls().size(); i++) {
		glPushMatrix();		
		glTranslatef(juego.getWalls()[i].getX(), juego.getWalls()[i].getY(), 0);
		glScalef(juego.getWalls()[i].getWidth(), juego.getWalls()[i].getHeight(), 0.5);
		glutSolidCube(1);
		glPopMatrix();
	}

}
//Se pintan los bloques
void igvEscena3D::pintarBloques() {

	GLfloat rojo[] = { 1,0,0,1.0 };
	GLfloat verde[] = { 0,1,0,1.0 };
	GLfloat azul[] = { 0,0,1,1.0 };

	for (int i = 0; i < juego.getBlocks().size(); i++) {
		if (juego.getBlocks()[i].isActive()) {
			glPushMatrix();
			if(juego.getBlocks()[i].blockId % 3 == 0) glMaterialfv(GL_FRONT, GL_EMISSION, rojo);
			else if (juego.getBlocks()[i].blockId % 3 == 1) glMaterialfv(GL_FRONT, GL_EMISSION, verde);
			else glMaterialfv(GL_FRONT, GL_EMISSION, azul);
			glTranslatef(juego.getBlocks()[i].getX(), juego.getBlocks()[i].getY(), 0);
			glScalef(juego.getBlocks()[i].getWidth(), juego.getBlocks()[i].getHeight(), 0.5);
			glutSolidCube(1);
			glPopMatrix();
		}
	}
}

// Pinta la pala para jugar
void igvEscena3D::pintarPala() {
	GLfloat rojo[] = { 1,0,0,1.0 };
	glPushMatrix();
	glTranslatef(juego.getPaddle().getBlock().getX(), juego.getPaddle().getBlock().getY(), 0);
	glScalef(juego.getPaddle().getBlock().getWidth(), juego.getPaddle().getBlock().getHeight(), 0.5);
	glMaterialfv(GL_FRONT, GL_EMISSION, rojo);
	glutSolidCube(1);
	glPopMatrix();
}