#pragma once
#include "Block.h"

Block::Block() {
	x = -10000;
	y = -10000;
}

//Este constructor se invoca para los bloques destruibles
Block::Block(float x, float y, float height, float width): x(x), y(y), height(height), width(width) {	
	blockId = ++id;
	active = true;
	normal = igvPunto3D(0,-1, 0);
	this->minimum = igvPunto3D(x - width / 2, y - height / 2, 0);
	this->maximum = igvPunto3D(x + width / 2, y + height / 2, 0);
}

//Este constructor se invoca para los bloques l�mite
Block::Block(float x, float y, float height, float width, igvPunto3D normal) : x(x), y(y), height(height), width(width), normal(normal){
	blockId = ++id;
	active = true;
	this->minimum = igvPunto3D(x - width / 2, y - height / 2, 0);
	this->maximum = igvPunto3D(x + width / 2, y + height / 2, 0);
}

void Block::Update() {
	this->minimum = igvPunto3D(x - width / 2, y - height / 2, 0);
	this->maximum = igvPunto3D(x + width / 2, y + height / 2, 0);
}