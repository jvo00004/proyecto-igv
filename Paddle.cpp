#include "Paddle.h"

Paddle::Paddle() {
	speed = 0.01;
	igvPunto3D normal(0, 1, 0);
	paddle = Block(0, -3, -0.2, 1, normal);
}

void Paddle::moveLeft(float deltaTime) {
	if (paddle.getX() > -1.35) {
		this->paddle.setX(paddle.getX() - speed * deltaTime);
		paddle.Update();
	}
}

void Paddle::moveRight(float deltaTime) {
	if (paddle.getX() < 1.35) {
		this->paddle.setX(paddle.getX() + speed * deltaTime);
		paddle.Update();
	}
}