#pragma once
#include "igvPunto3D.h"

class Block
{
private:
	//Si el bloque est� inactivo, no se considera a la hora de trabajar con las colisiones y no se pinta
	bool active;	
	//Posici�n del bloque
	float x;
	float y;
	//Tama�o del bloque
	float height;
	float width;
	//Vector Normal del bloque (utilizada para los rebotes)
	igvPunto3D normal;
	//Esquinas inferior izquierda y superior derecha, utilizadas para comprobar las colisiones
	igvPunto3D minimum;
	igvPunto3D maximum;


public:	
	Block();
	Block(float x, float y, float height, float width);
	Block(float x, float y, float height, float width, igvPunto3D normal);

	static int id;

	int blockId;

	float getX() const { return x; }
	float getY() const { return y; }
	float getHeight() const { return height; };
	float getWidth() const { return width; };
	//int getId() { return id; };

	float left()   const { return x - width/2; }
	float rigth()  const { return x + width/2; }
	float top()    const { return y + height/2; }
	float bottom() const { return y - height/2; }

	igvPunto3D mini() const { return minimum; };
	igvPunto3D maxi() const { return maximum; };
	igvPunto3D getNormal() const { return normal; };
	void setNormal(igvPunto3D normal) { this->normal = normal; };

	void setActive(bool activo) { active = activo; };
	bool isActive() { return active; }
	void setX(float pos) { x = pos; };
	void setY(float pos) { y = pos; };
	void Update();
};
