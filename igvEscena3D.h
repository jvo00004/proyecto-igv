#ifndef __IGVESCENA3D
#define __IGVESCENA3D

#if defined(__APPLE__) && defined(__MACH__)
#include <GLUT/glut.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/glut.h>
#endif

#include "igvFuenteLuz.h"
#include "Game.h"


#include <iostream>
using namespace std;

class igvEscena3D {
	protected:
		// Atributos
		Game juego;	
		igvFuenteLuz nuevaLuz;
	public:

		// Constructores por defecto y destructor
		igvEscena3D();
		~igvEscena3D();

		// M�todos
		// m�todo con las llamadas OpenGL para visualizar la escena
		void visualizar();

		// m�todo para crear la luz en la escena
		void luzDireccional();
		// m�todos para pintar los objetos del juego en la pantalla
		void pintarBola();
		void pintarPala();
		void pintarBloques();
		void pintarMuros();

		Game& getJuego() { return juego; };
};

#endif
